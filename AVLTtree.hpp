//
//  AVLTtree.hpp
//  CS130a_Project3
//
//  Created by Antonio Medina on 12/16/20.
//  Copyright © 2020 Antonio Medina. All rights reserved.
//

#ifndef AVLTtree_hpp
#define AVLTtree_hpp

#include <iostream>
#include <string>
//#include <sstream>
#include <stdint.h>

class AVLTree{
  
    
public:
  
    struct node{
          

          int wholePart;
          int fraction;
          int height;
          
          node *left;
          node *right;
         // node *parent;
          

    //
          node( int &&wp, int &&frac, node *lt, node *rt, int h= 0 )
          : wholePart( std:: move(wp) ), fraction( std:: move(frac) ), left( lt ), right( rt ), height( h ) { }

          node( const int & WP, const int & f, node *lt, node *rt, int h = 0 )
          : wholePart( WP ), fraction( f ),  left( lt ), right( rt ), height( h ) { }
        
//        ~node(){
//            delete left;
//            delete right;
//        }
    //
          //node();
          //node(){};
        
//        node(){
//            height= NULL;
//            wholePart= NULL;
//            fraction= NULL;
//            left= nullptr;
//            right= nullptr;
//        }
//        node(int WP, int f, node *l, node *r){
//            height= 0;
//            wholePart= WP;
//            fraction= f;
//            left= l;
//            right= r;
//        }
     };


    
    AVLTree(int k);
    ~AVLTree();
    void insert(int &wholeP, int &frac);
    void remove(int &wholeP, int &frac);
    void search(int &wholeP, int &frac);
    void approx_search(int &wholeP, int &frac);
    void inOrder();
    void preOrder();
    
    
private:
    
    node *findMax( node *head );
    int returnHeight(node *&head);
    void balance(node *&head);
    void insertUtil( int &wholeP, int &frac, node *&head);
    
    void removeUtil( int &wholeP, int &frac, node *&head);
    bool searchUtil( int &wholeP, int &frac, node *head);
    void approx_searchUtil(node *head);
    
    void rotateWithLeftChild(node *&head);
    void doubleWithLeft(node *&head);
    void rotateWithRightChild ( node *&head );
    void doubleWithRight ( node *&head );
    
    void inorderUtil(node *&head);
    void preorderUtil(node *&head);
    node *root;
    node *tempApproxNode;
    int kFactor;
    int minDifference;
    int approxKey;
    std::string temp;
    int count;
    
    void makeEmpty( node *head);
    
};
#endif /* AVLTtree_hpp */
