//
//  main.cpp
//  CS130a_Project3
//
//  Created by Antonio Medina on 12/12/20.
//  Copyright © 2020 Antonio Medina. All rights reserved.
//


#include <iostream>
#include <string>
#include <sstream>
#include "AVLTtree.hpp"


int main(int argc, char * argv[]) {

    int kAVLTree;
    
    std::string inputStr= argv[1];
    std::string command;
    int stringSize= inputStr.size();

    int wholePart;
    int fraction;
    
    std::stringstream ss(inputStr);
    
    ss>>kAVLTree;
             //std::cout<< kAVLTree<<std::endl;


    ss>>command;
    
    AVLTree kAVL(kAVLTree);
    
    for (int i = 0; i < stringSize; i++){
          
        if (ss>>command){
          //  std::cout<<"This command "<<command<< std::endl;
            if( command.size() == 8 || command.size()== 9 || command.size()== 10){
                
                if (command.substr(0,8) == "in_order"){
                    //std::cout<<command.substr(0,8)<<std::endl;
                    kAVL.inOrder();
                }
                else if (command.substr(0,9) == "pre_order"){
                    //std::cout<<command.substr(0,9)<<std::endl;
                    //ss>>command;
                    kAVL.preOrder();
                }
            }
            
            else if(command == "search"){
                if (ss >> wholePart >> fraction){
                    //std::cout << command << ": " << wholePart << "." << fraction << std::endl;
                }
                ss>>command;
                kAVL.search( wholePart, fraction);
            }

             else if(command == "approx_search"){
                if (ss >> wholePart >> fraction){
                    //std::cout << command << ": " << wholePart << "." << fraction << std::endl;
                }
                ss>>command;
                 kAVL.approx_search(wholePart, fraction);
            }
             else if (command == "insert"){
                if (ss >> wholePart >>fraction){
                    //std::cout << command << ": " << wholePart << "." << fraction<< std::endl;
                }
                
                 ss>>command;
                 kAVL.insert(wholePart, fraction);
                 //std::cout << wholePart << "." << fraction << " inserted" << std::endl;
            }

             else if (command == "delete"){
                if (ss >> wholePart >>fraction){
                    //std::cout << command << ": " << wholePart << "." << fraction<< std::endl;
                }
                ss>>command;
                kAVL.remove( wholePart, fraction );
            }

             
//            if (command == "in_order"){
//                std::cout<<command<<std::endl;
//             //   ss>>command;
//            }
            
//            if (command == "pre_order"){
//                std::cout<<command<<std::endl;
//              //  ss>>command;
//            }
                     
            
        }
           
    }
    return 0;
    
}


