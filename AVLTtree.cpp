//
//  AVLTtree.cpp
//
//  Created by Antonio Medina on 12/16/20.
//  Copyright © 2020 Antonio Medina. All rights reserved.
//

#include "AVLTtree.hpp"

/*
 Constructor to initialize the root of the empty tree and the kValue.
*/

AVLTree::AVLTree(int k){
    
    kFactor = k;
    count = 0;
    root = nullptr;
}

/*
 Returns the height of the tree.
*/
int AVLTree::returnHeight(node *&head){
    if( head == nullptr )
        return -1;
    else
        return head->height;
}

/*
 Helper function that returns the minimum value in the subtree.
*/
AVLTree::node *AVLTree::findMax(node *head){

    node *current= head;
    while ( current->right != nullptr) current= current->right;

    return current;
}

/*
 Function to balance the AVL tree after a deletion or insertion
*/
void AVLTree::balance(node *&head){

    if ( head == nullptr){
        return;
    }
    // If left subtree height is greater than kFactor.
    if( ( returnHeight( head->left ) - returnHeight( head->right ) ) > this->kFactor ){
        //Single rotation
        if( returnHeight( head->left->left ) >= returnHeight( head->left->right ))
            rotateWithLeftChild(head);
        //Double
        else
            doubleWithLeft( head );
    }
       
       // If right subtree height is greater than kFactor
    if( (returnHeight( head->right ) - returnHeight(head->left) ) > this->kFactor ){
        if( returnHeight( head->right->right ) >= returnHeight( head->right->left ) )
            rotateWithRightChild( head );

        else
            doubleWithRight( head );
        
    }
         
    //Update the heights of the tree after rotation.
    head->height= 1 + std::max(returnHeight(head->left), returnHeight(head->right));
        
       
}

/*
 Functions to insert a new node in the tree
*/
void AVLTree::insert(int &wholeP, int &frac){
    //If tree is empty, Add new node as the root  
    insertUtil(wholeP, frac, this->root);
}


void AVLTree::insertUtil(int &wholeP, int &frac, node *&head){
    
    //If tree is empty
    if ( head== nullptr ){
        if(this->count == 0){
            head= new node( wholeP, frac, nullptr, nullptr );
            this->root= head;
        }
        else
            head= new node( wholeP, frac, nullptr, nullptr );
        
        this->count = this->count +1;
        std::cout << wholeP << "." << frac << " inserted" << std::endl;
    }
    
    //Check where to insert new node
    else if( wholeP < head->wholePart) insertUtil(wholeP, frac, head->left);
    
    else if( wholeP > head->wholePart ){
        
        insertUtil(wholeP, frac, head->right);
    }
    
    else if( wholeP == head->wholePart){
        if( frac < head->fraction ) insertUtil(wholeP, frac, head->left );
        else if( frac > head->fraction ) insertUtil(wholeP, frac, head->right);
    }
    
  // balance the tree after node insertion.
    balance( head );

}

/*
 Left and Right single rotations.
*/
void AVLTree::rotateWithLeftChild(node *&head){
    
    node *temp= head->left;
    head->left = temp->right;
    temp->right = head;
    head->height = 1 + std::max( returnHeight( head->left ), returnHeight( head->right ));
    temp->height = 1 + std::max( returnHeight( temp->left ), head->height);
    head = temp;
}




void AVLTree::rotateWithRightChild(node *&head){
    
    node *temp= head->right;
    head->right = temp->left;
    temp->left = head;
    head->height = 1 + std::max( returnHeight( head->left ), returnHeight( head->right ));
    temp->height = 1 + std::max( returnHeight( temp->right ), head->height);
    head = temp;
}

/*
 Double rotations for left and right.
 */
void AVLTree::doubleWithLeft(node *&head){

    rotateWithRightChild( head->left );
    rotateWithLeftChild( head );
    
}

void AVLTree::doubleWithRight(node *&head){
    
    rotateWithLeftChild( head->right );
    rotateWithRightChild( head );
    
}
/*
 Functions to delete a node in the subtree.
*/

void AVLTree::remove(int &wholeP, int &frac){
    removeUtil( wholeP, frac, this->root);
}

void AVLTree::removeUtil(int &wholeP, int &frac, node *&head){

    if( head == nullptr )
        return;


    if( wholeP < head->wholePart || (wholeP == head->wholePart && frac < head->fraction))
        removeUtil(wholeP, frac, head->left);


    else if ( wholeP > head->wholePart || (wholeP == head->wholePart && frac > head->fraction) )
        removeUtil(wholeP, frac, head->right);
/*
 Two children case, replace the node with the inorder predecessor
 */
    else if( head->left != nullptr && head->right != nullptr ){
        node *temp = findMax(head->left);
        head->wholePart= temp->wholePart;
        head->fraction= temp->fraction;

        temp->wholePart = wholeP;
        temp->fraction = frac;

        removeUtil( wholeP, frac , head->left );
    }

    else{
        std::cout<<head->wholePart <<"."<< head->fraction << " deleted" <<std::endl;
        node *temp = head;
        if( head->left != nullptr ) head = head->left;
        else if ( head->right != nullptr ) head = head->right;
        else if ( head->right == nullptr && head->left == nullptr){
            // delete head;
            head = nullptr;
        }
        delete temp;
        temp = nullptr;
        this->count = this->count - 1;

    balance ( head );

}


/*
 Functions to search for a node in the AVL tree.
*/
void AVLTree::search(int &wholeP, int &frac){

    searchUtil( wholeP,  frac, this->root);
}

bool AVLTree::searchUtil(int &wholeP, int &frac, node *head){
    
    //Base
    if ( head == nullptr ){
        std::cout<< wholeP << "." << frac << " not found" << std::endl;
        return false;
    }
    //Traverse left subtree
    else if ( wholeP < head->wholePart )
        return searchUtil(wholeP, frac, head->left);
    //Travese right subtree
    else if ( wholeP > head->wholePart )
        return searchUtil(wholeP, frac, head->right);
    
    else {
        //left subtree
        if ( frac < head->fraction )
            return searchUtil(wholeP, frac, head->left);
        //right subtree
        else if ( frac > head->fraction )
            return searchUtil(wholeP, frac, head->right);
        //Found
        else{
            std::cout<< wholeP << "." << frac << " found" << std::endl;
            return true;
        }
        
    }
    
}


/*
  Functions for approximate search
*/
void AVLTree::approx_search(int &wholeP, int &frac){
    
    minDifference= INT32_MAX;
    
    //Convert key value to a whole int.
    approxKey= (wholeP*10) +frac;
    
    approx_searchUtil(root);
    
    if( minDifference != INT32_MAX){
        std::cout<< "closest to "  << wholeP <<"." << frac << " is " << tempApproxNode->wholePart<<"." << tempApproxNode->fraction<<std::endl;
    }
    
}

void AVLTree::approx_searchUtil(node *head){
    
    //Base
    if(head == nullptr) return;
    
    int tempKey= (head->wholePart*10) +head->fraction;
    int tempD= abs(approxKey - tempKey );
    
    if( tempD< minDifference){
        minDifference= tempD;
        tempApproxNode= head;
    }
    
    approx_searchUtil(head->left);
    approx_searchUtil(head->right);
}

/*
 Inorder and preorder traversals
 */
void AVLTree::inOrder(){

    if(this->root == nullptr) return;
    inorderUtil(this->root);
    temp.pop_back();
    std::cout <<temp;
    temp.clear();
    std::cout<<std::endl;
  
}

void AVLTree::inorderUtil(node *&head){
    
    if( head==nullptr ) return ;
    inorderUtil(head->left);
//
    temp.push_back((char)(head->wholePart+ '0'));
    temp.push_back(46);
    temp.push_back((char)(head->fraction + '0'));
    temp.push_back(32);
 
    inorderUtil(head->right);
    
}


void AVLTree::preOrder(){
    if(this->root == nullptr) return;
    preorderUtil(this->root);
    temp.pop_back();
    std::cout << temp;
    temp.clear();
    std::cout<<std::endl;
}

void AVLTree::preorderUtil(node *&head){
    
    if ( head==nullptr) return;

    temp.push_back((char)(head->wholePart+ '0'));
    temp.push_back(46);
    temp.push_back((char)(head->fraction + '0'));
    temp.push_back(32);
    preorderUtil( head->left );
    preorderUtil( head->right );
}

//Destructor

AVLTree::~AVLTree(){
    makeEmpty(this->root);
}

void AVLTree::makeEmpty( node* head){
    if(head != nullptr){
        makeEmpty( head->left);
        makeEmpty( head->right);
        delete head;
    }
    head= nullptr;
}
